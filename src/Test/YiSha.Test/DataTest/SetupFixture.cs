﻿using NUnit.Framework;
using YiSha.Util;
using YiSha.Util.Model;

namespace YiSha.DataTest
{
    [SetUpFixture]
    public class SetupFixture
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            GlobalContext.SystemConfig = new SystemConfig
            {
                //Mssql
                DBConnectionString = "server=localhost;uid=sa;pwd=123456;database=YiShaAdmin;MultipleActiveResultSets=true;TrustServerCertificate=true;",
                //DBConnectionString= "server=localhost;database=YiShaAdmin;Integrated Security=true;MultipleActiveResultSets=true;TrustServerCertificate=true;",
                //Mysql
                //DBConnectionString = "server=localhost;database=YiShaAdmin;user=root;password=123456;port=3306;",
                DBCommandTimeout = 180,
                DBBackup = "DataBase"
            };
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {

        }
    }
}
