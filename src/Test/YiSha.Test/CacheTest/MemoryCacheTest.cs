﻿using NUnit.Framework;
using System.Diagnostics;
using YiSha.Business.SystemManage;
using YiSha.Cache;
using YiSha.Entity;
using YiSha.Util;
using YiSha.Util.Model;

namespace YiSha.Test.CacheTest
{
    public class MemoryCacheTest
    {
        [SetUp]
        public void Init()
        {
            GlobalContext.SystemConfig = new SystemConfig
            {
                CacheProvider = "Memory",

                DBConnectionString= "server=localhost;uid=sa;pwd=123456;database=YiShaAdmin;MultipleActiveResultSets=true;TrustServerCertificate=true;",
                //DBConnectionString= "server=localhost;database=YiShaAdmin;Integrated Security=true;MultipleActiveResultSets=true;TrustServerCertificate=true;",
                DBCommandTimeout = 1000,
            };
        }

        [Test]
        public void TestRedisSimple()
        {
            string key = "test_simple_key";
            string value = "test_simple_value";
            CacheFactory.Cache.SetCache<string>(key, value);

            Assert.AreEqual(value, CacheFactory.Cache.GetCache<string>(key));
        }

        [Test]
        public void TestRedisComplex()
        {
            string key = "test_complex_key";
            TData<string> value = new TData<string> { Tag = 1, Data = "测试Memory" };
            CacheFactory.Cache.SetCache<TData<string>>(key, value);

            var result = CacheFactory.Cache.GetCache<TData<string>>(key);
            if (result.Tag == 1)
            {
                Assert.Pass(nameof(TestRedisComplex));
            }
            else
            {
                Assert.Fail(nameof(TestRedisComplex));
            }
        }

        [Test]
        public async Task TestRedisPerformance()
        {
            LogLoginBLL logLoginBLL = new LogLoginBLL();
            var obj = await logLoginBLL.GetList(null);

            string key = "test_performance_key";
            Stopwatch sw = new Stopwatch();
            sw.Start();
            CacheFactory.Cache.SetCache<List<LogLoginEntity>>(key, obj.Data);
            sw.Stop();
            Console.WriteLine(nameof(TestRedisPerformance) + " Redis Write Time:" + sw.ElapsedMilliseconds + " ms");

            sw.Restart();
            var result = CacheFactory.Cache.GetCache<List<LogLoginEntity>>(key);
            sw.Stop();
            if (obj.Data.Count == result.Count)
            {
                Console.WriteLine(nameof(TestRedisPerformance) + " Redis Read Time:" + sw.ElapsedMilliseconds + " ms");
            }
            else
            {
                Assert.Fail(nameof(TestRedisPerformance));
            }
        }
    }
}