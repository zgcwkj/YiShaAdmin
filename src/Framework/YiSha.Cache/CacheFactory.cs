﻿using YiSha.Util;

namespace YiSha.Cache
{
    public class CacheFactory
    {
        private static ICache cache = null;
        private static readonly object lockHelper = new object();

        public static ICache Cache
        {
            get
            {
                if (cache != null) return cache;
                lock (lockHelper)
                {
                    if (cache != null) return cache;
                    //
                    cache = GlobalContext.SystemConfig.CacheProvider switch
                    {
                        "Redis" => new RedisCacheImp(),
                        _ => new MemoryCacheImp(),
                    };
                }
                return cache;
            }
        }
    }
}
