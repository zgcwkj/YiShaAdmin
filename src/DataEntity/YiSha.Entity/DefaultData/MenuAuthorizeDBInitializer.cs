﻿namespace YiSha.Entity
{
    /// <summary>
    /// 菜单权限表数据初始化
    /// </summary>
    internal class MenuAuthorizeDBInitializer
    {
        /// <summary>
        /// 获取数据
        /// </summary>
        public static List<MenuAuthorizeEntity> GetData
        {
            get
            {
                var lists = new List<MenuAuthorizeEntity>();

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199565000704,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130069,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199573389312,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130072,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199577583616,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130084,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199585972224,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130085,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199590166528,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130086,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199598555136,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130087,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199602749440,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130088,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199611138048,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130089,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199615332352,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130073,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199623720960,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130090,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199627915264,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130091,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199636303872,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130092,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199640498176,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130093,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199648886784,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130083,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199653081088,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130094,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199657275392,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130095,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199665664000,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130096,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199669858304,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130097,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199678246912,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130129,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199686635520,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130130,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199695024128,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130131,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199699218432,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130132,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199707607040,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130133,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199715995648,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130070,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199720189952,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130074,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199728578560,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130098,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199732772864,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130099,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199741161472,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130100,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199753744384,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130101,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199757938688,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130075,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199766327296,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130102,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199774715904,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130103,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199783104512,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130104,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199787298816,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130105,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199795687424,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130077,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199804076032,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130106,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199812464640,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130107,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199825047552,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130108,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199833436160,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130109,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199841824768,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130078,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199850213376,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130110,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199858601984,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130111,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199866990592,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130112,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199875379200,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130113,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199883767808,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130076,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199892156416,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130082,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199904739328,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130114,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199913127936,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130116,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199921516544,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130115,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199929905152,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130117,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199938293760,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130081,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199946682368,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130135,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199955070976,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130136,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199967653888,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130137,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199976042496,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130122,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199984431104,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130123,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485199992819712,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130124,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200001208320,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130125,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200009596928,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130126,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200022179840,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130127,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200030568448,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130128,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200038957056,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130134,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200051539968,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130079,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200059928576,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130118,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200068317184,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130071,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200080900096,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130080,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200089288704,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130120,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200101871616,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130119,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 21485200110260224,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130121,
                    AuthorizeId = 16508640061130146,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 95470719411949568,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130069,
                    AuthorizeId = 16508640061130147,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 95470719458086912,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130072,
                    AuthorizeId = 16508640061130147,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 95470719483252736,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130084,
                    AuthorizeId = 16508640061130147,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 95470719483252737,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130085,
                    AuthorizeId = 16508640061130147,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241783234560,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130069,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241787428864,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130072,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241791623168,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130084,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241800011776,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130085,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241804206080,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130086,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241808400384,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130087,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241812594688,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130088,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241820983296,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130089,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241825177600,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130073,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241829371904,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130090,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241837760512,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130091,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241841954816,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130092,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241850343424,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130093,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241854537728,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130083,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241862926336,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130094,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241871314944,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130095,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241875509248,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130096,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241883897856,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130097,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241892286464,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130129,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241900675072,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130130,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241909063680,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130131,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241913257984,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130132,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1,
                });

                lists.Add(new MenuAuthorizeEntity
                {
                    Id = 103195241921646592,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 16508640061130151,
                    MenuId = 16508640061130133,
                    AuthorizeId = 103195209390624768,
                    AuthorizeType = 1
                });

                return lists;
            }
        }
    }
}
