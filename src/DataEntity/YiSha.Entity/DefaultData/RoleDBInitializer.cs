﻿namespace YiSha.Entity
{
    /// <summary>
    /// 角色表数据初始化
    /// </summary>
    internal class RoleDBInitializer
    {
        /// <summary>
        /// 获取数据
        /// </summary>
        public static List<RoleEntity> GetData
        {
            get
            {
                var lists = new List<RoleEntity>();

                lists.Add(new RoleEntity()
                {
                    Id = 16508640061130146,
                    BaseIsDelete = 0,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseModifyTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 0,
                    BaseModifierId = 16508640061130151,
                    BaseVersion = 0,
                    RoleName = "管理员",
                    RoleSort = 1,
                    RoleStatus = 1,
                    Remark = "管理员角色",
                });

                lists.Add(new RoleEntity()
                {
                    Id = 16508640061130147,
                    BaseIsDelete = 0,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseModifyTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 0,
                    BaseModifierId = 16508640061130151,
                    BaseVersion = 0,
                    RoleName = "普通角色",
                    RoleSort = 2,
                    RoleStatus = 1,
                    Remark = "普通角色",
                });

                return lists;
            }
        }
    }
}
