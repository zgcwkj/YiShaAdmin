﻿namespace YiSha.Entity
{
    /// <summary>
    /// 字典类型表数据初始化
    /// </summary>
    internal class DataDictDBInitializer
    {
        /// <summary>
        /// 获取数据
        /// </summary>
        public static List<DataDictEntity> GetData
        {
            get
            {
                var lists = new List<DataDictEntity>();

                lists.Add(new DataDictEntity()
                {
                    Id = 16508640061124399,
                    BaseIsDelete = 0,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseModifyTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 0,
                    BaseModifierId = 0,
                    BaseVersion = 0,
                    DictType = "NewsType",
                    DictSort = 0,
                    Remark = "新闻类别",
                });

                return lists;
            }
        }
    }
}
