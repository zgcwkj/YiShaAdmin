﻿namespace YiSha.Entity
{
    /// <summary>
    /// 定时任务表数据初始化
    /// </summary>
    internal class AutoJobDBInitializer
    {
        /// <summary>
        /// 获取数据
        /// </summary>
        public static List<AutoJobEntity> GetData
        {
            get
            {
                var lists = new List<AutoJobEntity>();

                lists.Add(new AutoJobEntity()
                {
                    Id = 16508640061124370,
                    BaseIsDelete = 0,
                    BaseCreateTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseModifyTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    BaseCreatorId = 0,
                    BaseModifierId = 0,
                    BaseVersion = 0,
                    JobGroupName = "YiShaAdmin",
                    JobName = "数据库备份",
                    JobStatus = 1,
                    CronExpression = "0 0 3 1/1 * ?",
                    StartTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    EndTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    NextStartTime = Convert.ToDateTime("2020-01-01 00:00:00"),
                    Remark = "",
                });

                return lists;
            }
        }
    }
}
