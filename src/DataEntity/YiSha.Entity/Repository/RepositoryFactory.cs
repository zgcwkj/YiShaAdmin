﻿using YiSha.Util;

namespace YiSha.Entity
{
    public class RepositoryFactory
    {
        public Repository BaseRepository()
        {
            //var dbConnectionString = GlobalContext.SystemConfig.DBConnectionString;
            //var database = new SqlServerDatabase(dbConnectionString);
            var database = new MyDbContext();
            return new Repository(database);
        }
    }
}
