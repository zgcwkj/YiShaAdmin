﻿using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using YiSha.Util.Model;
using Microsoft.EntityFrameworkCore.Metadata;
using StackExchange.Redis.KeyspaceIsolation;
using System.Collections;
using System.Text;
using YiSha.Util.Extension;
using NPOI.SS.Formula.Functions;
using Org.BouncyCastle.Crypto;

namespace YiSha.Entity
{
    /// <summary>
    /// 定义仓储模型中的数据标准操作接口
    /// </summary>
    public class Repository
    {
        public MyDbContext dbContext { get; }

        public Repository(MyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        #region 事务提交

        public async Task<Repository> BeginTrans()
        {
            await this.BeginTransUp();
            return this;
        }

        public async Task<int> CommitTrans()
        {
            return await this.CommitTransUp();
        }

        public async Task RollbackTrans()
        {
            await this.RollbackTransUp();
        }

        #endregion 事务提交

        #region 执行 SQL 语句

        public async Task<int> ExecuteBySql(string strSql)
        {
            return await this.ExecuteBySqlUp(strSql);
        }

        public async Task<int> ExecuteBySql(string strSql, params DbParameter[] dbParameter)
        {
            return await this.ExecuteBySqlUp(strSql, dbParameter);
        }

        public async Task<int> ExecuteByProc(string procName)
        {
            return await this.ExecuteByProcUp(procName);
        }

        public async Task<int> ExecuteByProc(string procName, params DbParameter[] dbParameter)
        {
            return await this.ExecuteByProcUp(procName, dbParameter);
        }

        #endregion 执行 SQL 语句

        #region 对象实体 添加、修改、删除

        public async Task<int> Insert<T>(T entity) where T : class
        {
            return await this.InsertUp<T>(entity);
        }

        public async Task<int> Insert<T>(List<T> entity) where T : class
        {
            return await this.InsertUp<T>(entity);
        }

        public async Task<int> Delete<T>() where T : class
        {
            return await this.DeleteUp<T>();
        }

        public async Task<int> Delete<T>(T entity) where T : class
        {
            return await this.DeleteUp<T>(entity);
        }

        public async Task<int> Delete<T>(List<T> entity) where T : class
        {
            return await this.DeleteUp<T>(entity);
        }

        public async Task<int> Delete<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return await this.DeleteUp<T>(condition);
        }

        public async Task<int> Delete<T>(long id) where T : class
        {
            return await this.DeleteUp<T>(id);
        }

        public async Task<int> Delete<T>(long[] id) where T : class
        {
            return await this.DeleteUp<T>(id);
        }

        public async Task<int> Delete<T>(string propertyName, long propertyValue) where T : class
        {
            return await this.DeleteUp<T>(propertyName, propertyValue);
        }

        public async Task<int> Update<T>(T entity) where T : class
        {
            return await this.UpdateUp<T>(entity);
        }

        public async Task<int> Update<T>(List<T> entity) where T : class
        {
            return await this.UpdateUp<T>(entity);
        }

        public async Task<int> UpdateAllField<T>(T entity) where T : class
        {
            return await this.UpdateAllFieldUp<T>(entity);
        }

        public async Task<int> Update<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return await this.UpdateUp<T>(condition);
        }

        public IQueryable<T> IQueryable<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return this.IQueryableUp<T>(condition);
        }

        #endregion 对象实体 添加、修改、删除

        #region 对象实体 查询

        public async Task<T> FindEntity<T>(long id) where T : class
        {
            return await this.FindEntityUp<T>(id);
        }

        public async Task<T> FindEntity<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return await this.FindEntityUp<T>(condition);
        }

        public async Task<IEnumerable<T>> FindList<T>() where T : class, new()
        {
            return await this.FindListUp<T>();
        }

        public async Task<IEnumerable<T>> FindList<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return await this.FindListUp<T>(condition);
        }

        public async Task<IEnumerable<T>> FindList<T>(string strSql) where T : class
        {
            return await this.FindListUp<T>(strSql);
        }

        public async Task<IEnumerable<T>> FindList<T>(string strSql, DbParameter[] dbParameter) where T : class
        {
            return await this.FindListUp<T>(strSql, dbParameter);
        }

        public async Task<(int total, IEnumerable<T> list)> FindList<T>(Pagination pagination) where T : class, new()
        {
            int total = pagination.TotalCount;
            var data = await this.FindListUp<T>(pagination.Sort, pagination.SortType.ToLower() == "asc" ? true : false, pagination.PageSize, pagination.PageIndex);
            pagination.TotalCount = total;
            return data;
        }

        public async Task<IEnumerable<T>> FindList<T>(Expression<Func<T, bool>> condition, Pagination pagination) where T : class, new()
        {
            var data = await this.FindListUp<T>(condition, pagination.Sort, pagination.SortType.ToLower() == "asc" ? true : false, pagination.PageSize, pagination.PageIndex);
            pagination.TotalCount = data.total;
            return data.list;
        }

        public async Task<(int total, IEnumerable<T> list)> FindList<T>(string strSql, Pagination pagination) where T : class
        {
            int total = pagination.TotalCount;
            var data = await this.FindListUp<T>(strSql, pagination.Sort, pagination.SortType.ToLower() == "asc" ? true : false, pagination.PageSize, pagination.PageIndex);
            pagination.TotalCount = total;
            return data;
        }

        public async Task<IEnumerable<T>> FindList<T>(string strSql, DbParameter[] dbParameter, Pagination pagination) where T : class
        {
            var data = await this.FindListUp<T>(strSql, dbParameter, pagination.Sort, pagination.SortType.ToLower() == "asc" ? true : false, pagination.PageSize, pagination.PageIndex);
            pagination.TotalCount = data.total;
            return data.Item2;
        }

        #endregion 对象实体 查询

        #region 数据源 查询

        public async Task<DataTable> FindTable(string strSql)
        {
            return await this.FindTableUp(strSql);
        }

        public async Task<DataTable> FindTable(string strSql, DbParameter[] dbParameter)
        {
            return await this.FindTableUp(strSql, dbParameter);
        }

        public async Task<DataTable> FindTable(string strSql, Pagination pagination)
        {
            var data = await this.FindTableUp(strSql, pagination.Sort, pagination.SortType.ToLower() == "asc" ? true : false, pagination.PageSize, pagination.PageIndex);
            pagination.TotalCount = data.total;
            return data.Item2;
        }

        public async Task<DataTable> FindTable(string strSql, DbParameter[] dbParameter, Pagination pagination)
        {
            var data = await this.FindTableUp(strSql, dbParameter, pagination.Sort, pagination.SortType.ToLower() == "asc" ? true : false, pagination.PageSize, pagination.PageIndex);
            pagination.TotalCount = data.total;
            return data.Item2;
        }

        public async Task<object> FindObject(string strSql)
        {
            return await this.FindObjectUp(strSql);
        }

        public async Task<object> FindObject(string strSql, DbParameter[] dbParameter)
        {
            return await this.FindObjectUp(strSql, dbParameter);
        }

        public async Task<T> FindObject<T>(string strSql) where T : class
        {
            return await this.FindObjectUp<T>(strSql);
        }

        #endregion 数据源 查询

        //==>

        /// <summary>
        /// 事务对象
        /// </summary>
        private IDbContextTransaction dbContextTransaction { get; set; }

        #region 事务提交

        /// <summary>
        /// 事务开始
        /// </summary>
        /// <returns></returns>
        private async Task<Repository> BeginTransUp()
        {
            DbConnection dbConnection = dbContext.Database.GetDbConnection();
            if (dbConnection.State == ConnectionState.Closed)
            {
                await dbConnection.OpenAsync();
            }
            dbContextTransaction = await dbContext.Database.BeginTransactionAsync();
            return this;
        }

        /// <summary>
        /// 提交当前操作的结果
        /// </summary>
        private async Task<int> CommitTransUp()
        {
            try
            {
                DbContextExtension.SetEntityDefaultValue(dbContext);

                int returnValue = await dbContext.SaveChangesAsync();
                if (dbContextTransaction != null)
                {
                    await dbContextTransaction.CommitAsync();
                    await this.CloseUp();
                }
                else
                {
                    await this.CloseUp();
                }
                return returnValue;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dbContextTransaction == null)
                {
                    await this.CloseUp();
                }
            }
        }

        /// <summary>
        /// 把当前操作回滚成未提交状态
        /// </summary>
        private async Task RollbackTransUp()
        {
            await this.dbContextTransaction.RollbackAsync();
            await this.dbContextTransaction.DisposeAsync();
            await this.CloseUp();
        }

        /// <summary>
        /// 关闭连接 内存回收
        /// </summary>
        private async Task CloseUp()
        {
            await dbContext.DisposeAsync();
        }

        #endregion 事务提交

        #region 执行 SQL 语句

        private async Task<int> ExecuteBySqlUp(string strSql)
        {
            if (dbContextTransaction == null)
            {
                return await dbContext.Database.ExecuteSqlRawAsync(strSql);
            }
            else
            {
                await dbContext.Database.ExecuteSqlRawAsync(strSql);
                return dbContextTransaction == null ? await this.CommitTransUp() : 0;
            }
        }

        private async Task<int> ExecuteBySqlUp(string strSql, params DbParameter[] dbParameter)
        {
            if (dbContextTransaction == null)
            {
                return await dbContext.Database.ExecuteSqlRawAsync(strSql, dbParameter);
            }
            else
            {
                await dbContext.Database.ExecuteSqlRawAsync(strSql, dbParameter);
                return dbContextTransaction == null ? await this.CommitTransUp() : 0;
            }
        }

        private async Task<int> ExecuteByProcUp(string procName)
        {
            if (dbContextTransaction == null)
            {
                return await dbContext.Database.ExecuteSqlRawAsync(DbContextExtension.BuilderProc(procName));
            }
            else
            {
                await dbContext.Database.ExecuteSqlRawAsync(DbContextExtension.BuilderProc(procName));
                return dbContextTransaction == null ? await this.CommitTransUp() : 0;
            }
        }

        private async Task<int> ExecuteByProcUp(string procName, params DbParameter[] dbParameter)
        {
            if (dbContextTransaction == null)
            {
                return await dbContext.Database.ExecuteSqlRawAsync(DbContextExtension.BuilderProc(procName, dbParameter), dbParameter);
            }
            else
            {
                await dbContext.Database.ExecuteSqlRawAsync(DbContextExtension.BuilderProc(procName, dbParameter), dbParameter);
                return dbContextTransaction == null ? await this.CommitTransUp() : 0;
            }
        }

        #endregion 执行 SQL 语句

        #region 对象实体 添加、修改、删除

        private async Task<int> InsertUp<T>(T entity) where T : class
        {
            dbContext.Entry<T>(entity).State = EntityState.Added;
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> InsertUp<T>(IEnumerable<T> entities) where T : class
        {
            foreach (var entity in entities)
            {
                dbContext.Entry<T>(entity).State = EntityState.Added;
            }
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> DeleteUp<T>() where T : class
        {
            IEntityType entityType = DbContextExtension.GetEntityType<T>(dbContext);
            if (entityType != null)
            {
                string tableName = entityType.GetTableName();
                return await this.ExecuteBySqlUp(DbContextExtension.DeleteSql(tableName));
            }
            return -1;
        }

        private async Task<int> DeleteUp<T>(T entity) where T : class
        {
            dbContext.Set<T>().Attach(entity);
            dbContext.Set<T>().Remove(entity);
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> DeleteUp<T>(IEnumerable<T> entities) where T : class
        {
            foreach (var entity in entities)
            {
                dbContext.Set<T>().Attach(entity);
                dbContext.Set<T>().Remove(entity);
            }
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> DeleteUp<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            IEnumerable<T> entities = await dbContext.Set<T>().Where(condition).ToListAsync();
            return entities.Count() > 0 ? await DeleteUp(entities) : 0;
        }

        private async Task<int> DeleteUp<T>(long keyValue) where T : class
        {
            IEntityType entityType = DbContextExtension.GetEntityType<T>(dbContext);
            if (entityType != null)
            {
                string tableName = entityType.GetTableName();
                string keyField = "Id";
                return await this.ExecuteBySqlUp(DbContextExtension.DeleteSql(tableName, keyField, keyValue));
            }
            return -1;
        }

        private async Task<int> DeleteUp<T>(long[] keyValue) where T : class
        {
            IEntityType entityType = DbContextExtension.GetEntityType<T>(dbContext);
            if (entityType != null)
            {
                string tableName = entityType.GetTableName();
                string keyField = "Id";
                return await this.ExecuteBySqlUp(DbContextExtension.DeleteSql(tableName, keyField, keyValue));
            }
            return -1;
        }

        private async Task<int> DeleteUp<T>(string propertyName, long propertyValue) where T : class
        {
            IEntityType entityType = DbContextExtension.GetEntityType<T>(dbContext);
            if (entityType != null)
            {
                string tableName = entityType.GetTableName();
                return await this.ExecuteBySqlUp(DbContextExtension.DeleteSql(tableName, propertyName, propertyValue));
            }
            return -1;
        }

        private async Task<int> UpdateUp<T>(T entity) where T : class
        {
            dbContext.Set<T>().Attach(entity);
            Hashtable props = DatabasesExtension.GetPropertyInfo<T>(entity);
            foreach (string item in props.Keys)
            {
                if (item == "Id")
                {
                    continue;
                }
                object value = dbContext.Entry(entity).Property(item).CurrentValue;
                if (value != null)
                {
                    dbContext.Entry(entity).Property(item).IsModified = true;
                }
            }
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> UpdateUp<T>(IEnumerable<T> entities) where T : class
        {
            foreach (var entity in entities)
            {
                dbContext.Entry<T>(entity).State = EntityState.Modified;
            }
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> UpdateAllFieldUp<T>(T entity) where T : class
        {
            dbContext.Set<T>().Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;
            return dbContextTransaction == null ? await this.CommitTransUp() : 0;
        }

        private async Task<int> UpdateUp<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            IEnumerable<T> entities = await dbContext.Set<T>().Where(condition).ToListAsync();
            return entities.Count() > 0 ? await UpdateUp(entities) : 0;
        }

        private IQueryable<T> IQueryableUp<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return dbContext.Set<T>().Where(condition);
        }

        #endregion 对象实体 添加、修改、删除

        #region 对象实体 查询

        private async Task<T> FindEntityUp<T>(object keyValue) where T : class
        {
            return await dbContext.Set<T>().FindAsync(keyValue);
        }

        private async Task<T> FindEntityUp<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return await dbContext.Set<T>().Where(condition).FirstOrDefaultAsync();
        }

        private async Task<IEnumerable<T>> FindListUp<T>() where T : class, new()
        {
            return await dbContext.Set<T>().ToListAsync();
        }

        private async Task<IEnumerable<T>> FindListUp<T>(Func<T, object> orderby) where T : class, new()
        {
            var list = await dbContext.Set<T>().ToListAsync();
            list = list.OrderBy(orderby).ToList();
            return list;
        }

        private async Task<IEnumerable<T>> FindListUp<T>(Expression<Func<T, bool>> condition) where T : class, new()
        {
            return await dbContext.Set<T>().Where(condition).ToListAsync();
        }

        private async Task<IEnumerable<T>> FindListUp<T>(string strSql) where T : class
        {
            return await FindListUp<T>(strSql, null);
        }

        private async Task<IEnumerable<T>> FindListUp<T>(string strSql, DbParameter[] dbParameter) where T : class
        {
            using (var dbConnection = dbContext.Database.GetDbConnection())
            {
                var reader = await new DbHelper(dbContext, dbConnection).ExecuteReadeAsync(CommandType.Text, strSql, dbParameter);
                return DatabasesExtension.IDataReaderToList<T>(reader);
            }
        }

        private async Task<(int total, IEnumerable<T> list)> FindListUp<T>(string sort, bool isAsc, int pageSize, int pageIndex) where T : class, new()
        {
            var tempData = dbContext.Set<T>().AsQueryable();
            return await FindListUp<T>(tempData, sort, isAsc, pageSize, pageIndex);
        }

        private async Task<(int total, IEnumerable<T> list)> FindListUp<T>(Expression<Func<T, bool>> condition, string sort, bool isAsc, int pageSize, int pageIndex) where T : class, new()
        {
            var tempData = dbContext.Set<T>().Where(condition);
            return await FindListUp<T>(tempData, sort, isAsc, pageSize, pageIndex);
        }

        private async Task<(int total, IEnumerable<T>)> FindListUp<T>(string strSql, string sort, bool isAsc, int pageSize, int pageIndex) where T : class
        {
            return await FindListUp<T>(strSql, null, sort, isAsc, pageSize, pageIndex);
        }

        private async Task<(int total, IEnumerable<T>)> FindListUp<T>(string strSql, DbParameter[] dbParameter, string sort, bool isAsc, int pageSize, int pageIndex) where T : class
        {
            using (var dbConnection = dbContext.Database.GetDbConnection())
            {
                DbHelper dbHelper = new DbHelper(dbContext, dbConnection);
                StringBuilder sb = new StringBuilder();
                sb.Append(DatabasePageExtension.SqlServerPageSql(strSql, dbParameter, sort, isAsc, pageSize, pageIndex));
                object tempTotal = await dbHelper.ExecuteScalarAsync(CommandType.Text, "SELECT COUNT(1) FROM (" + strSql + ") T", dbParameter);
                int total = tempTotal.ParseToInt();
                if (total > 0)
                {
                    var reader = await dbHelper.ExecuteReadeAsync(CommandType.Text, sb.ToString(), dbParameter);
                    return (total, DatabasesExtension.IDataReaderToList<T>(reader));
                }
                else
                {
                    return (total, new List<T>());
                }
            }
        }

        private async Task<(int total, IEnumerable<T> list)> FindListUp<T>(IQueryable<T> tempData, string sort, bool isAsc, int pageSize, int pageIndex)
        {
            tempData = DatabasesExtension.AppendSort<T>(tempData, sort, isAsc);
            var total = tempData.Count();
            if (total > 0)
            {
                tempData = tempData.Skip<T>(pageSize * (pageIndex - 1)).Take<T>(pageSize).AsQueryable();
                var list = await tempData.ToListAsync();
                return (total, list);
            }
            else
            {
                return (total, new List<T>());
            }
        }

        #endregion 对象实体 查询

        #region 数据源查询

        private async Task<DataTable> FindTableUp(string strSql)
        {
            return await FindTableUp(strSql, null);
        }

        private async Task<DataTable> FindTableUp(string strSql, DbParameter[] dbParameter)
        {
            using (var dbConnection = dbContext.Database.GetDbConnection())
            {
                var reader = await new DbHelper(dbContext, dbConnection).ExecuteReadeAsync(CommandType.Text, strSql, dbParameter);
                return DatabasesExtension.IDataReaderToDataTable(reader);
            }
        }

        private async Task<(int total, DataTable)> FindTableUp(string strSql, string sort, bool isAsc, int pageSize, int pageIndex)
        {
            return await FindTableUp(strSql, null, sort, isAsc, pageSize, pageIndex);
        }

        private async Task<(int total, DataTable)> FindTableUp(string strSql, DbParameter[] dbParameter, string sort, bool isAsc, int pageSize, int pageIndex)
        {
            using (var dbConnection = dbContext.Database.GetDbConnection())
            {
                DbHelper dbHelper = new DbHelper(dbContext, dbConnection);
                StringBuilder sb = new StringBuilder();
                sb.Append(DatabasePageExtension.SqlServerPageSql(strSql, dbParameter, sort, isAsc, pageSize, pageIndex));
                object tempTotal = await dbHelper.ExecuteScalarAsync(CommandType.Text, "SELECT COUNT(1) FROM (" + strSql + ") T", dbParameter);
                int total = tempTotal.ParseToInt();
                if (total > 0)
                {
                    var reader = await dbHelper.ExecuteReadeAsync(CommandType.Text, sb.ToString(), dbParameter);
                    DataTable resultTable = DatabasesExtension.IDataReaderToDataTable(reader);
                    return (total, resultTable);
                }
                else
                {
                    return (total, new DataTable());
                }
            }
        }

        private async Task<object> FindObjectUp(string strSql)
        {
            return await FindObjectUp(strSql, null);
        }

        private async Task<object> FindObjectUp(string strSql, DbParameter[] dbParameter)
        {
            using (var dbConnection = dbContext.Database.GetDbConnection())
            {
                return await new DbHelper(dbContext, dbConnection).ExecuteScalarAsync(CommandType.Text, strSql, dbParameter);
            }
        }

        private async Task<T> FindObjectUp<T>(string strSql) where T : class
        {
            var list = await dbContext.SqlQuery<T>(strSql);
            return list.FirstOrDefault();
        }

        #endregion 数据源查询
    }
}
