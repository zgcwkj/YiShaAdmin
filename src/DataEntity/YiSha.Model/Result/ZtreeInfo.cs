﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YiSha.Util;

namespace YiSha.Model.Result
{
    public class ZtreeInfo
    {
        public long? id { get; set; }

        public long? pId { get; set; }

        public string name { get; set; }
    }
}
